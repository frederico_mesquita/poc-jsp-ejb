<%@page import="br.com.papodecafeteria.dao.jpa.UserDaoJpa"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>  
<jsp:useBean id="pUser" class="br.com.papodecafeteria.dao.model.UserJPA"></jsp:useBean>
<jsp:setProperty property="*" name="pUser"/>  
  
<%
	int status = 0;

	try{
		status = UserDaoJpa.update(pUser);
	} catch(Exception exc){
		Logger.getLogger("edituser").log(Level.SEVERE, exc.getMessage());
	}
	response.sendRedirect("viewusers.jsp");  
%>  