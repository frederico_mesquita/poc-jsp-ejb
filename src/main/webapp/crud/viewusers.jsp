<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
			$(".restore").fadeOut();
		    $(".link").click(function(){
		        $("#home").fadeOut();
		    });
		});
		
		$(document).ready(function(){
			$(".line").click(function(){
		        $(this).slideUp();
		        $(".restore").fadeIn();
		    });
			$(".restore").click(function(){
				$(".line").slideDown();
		        $(".restore").fadeOut();
		    });
		});
		</script>
		<title>View Users</title>
	</head>
	<body>
		<div align="center" id="viewusers">
			<%@page import="br.com.papodecafeteria.dao.jpa.UserDaoJpa"%>
			<%@page import="br.com.papodecafeteria.dao.model.UserJPA"%>
			<%@page import="java.util.logging.Logger"%>
			<%@page import="java.util.logging.Level"%>
			<%@page import="java.util.List"%>    
			<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
			<h1>List of Users</h1>
			<%
				List<UserJPA> lstUserJPA = null;
			
				try{
					lstUserJPA = UserDaoJpa.getAllRecords();
				} catch(Exception exc){
					Logger.getLogger("viewusers").log(Level.SEVERE, exc.getMessage());
				}
				request.setAttribute("lstUserJPA", lstUserJPA);
			%>
			<table border="1" width="90%" cellpadding="10">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Sex</th>
						<th>Country</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<c:forEach items="${lstUserJPA}" var="userJpa">  
				<tr id="line" class="line">
					<td align="center">${userJpa.getId()}</td>
					<td id="name">${userJpa.getName()}</td> 
					<td id="email">${userJpa.getEmail()}</td>
					<td id="sex" align="center">${userJpa.getSex()}</td>
					<td id="country">${userJpa.getCountry()}</td>  
					<td id="edit" align="center"><a href="editform.jsp?id=${userJpa.getId()}" class="link">Edit</a></td>  
					<td id="delete" align="center"><a href="deleteuser.jsp?id=${userJpa.getId()}" class="link">Delete</a></td>
				</tr>  
				</c:forEach>  
			</table>  
			<br/>
			<p>
				<button id="adduser" onclick="location.href='adduserform.jsp'" type="button">Add User</button>
				<button id="home" onclick="location.href='../index.jsp'" type="button">Home</button>
			</p>
			<p class="restore">Click me to restores table�s lines.</p>
		</div>
	</body>
</html>