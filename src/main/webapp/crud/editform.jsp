<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit Form</title>
	</head>
	<body>
		<%@page import="br.com.papodecafeteria.dao.jpa.UserDaoJpa"%>
		<%@page import="br.com.papodecafeteria.dao.model.UserJPA"%>
		<%@page import="java.util.logging.Logger"%>
		<%@page import="java.util.logging.Level"%>
		<%  
			String id = request.getParameter("id");
		
			UserJPA userJPA = null;
			try{
				userJPA = UserDaoJpa.getRecordById(Integer.parseInt(id));
			} catch(Exception exc){
				Logger.getLogger("editform").log(Level.SEVERE, exc.getMessage());
			}  
		%>
		<div align="center" id="edituser">
			<h1>Edit Form</h1>  
			<form id="edituserform" action="edituser.jsp" method="post">  
				<input type="hidden" name="id" value="<%= userJPA.getId() %>"/>  
				<table>  
					<tr><td>Name:</td><td><input type="text" id="name" name="name" value="<%= userJPA.getName()%>"/></td></tr>  
					<tr><td>Email:</td><td><input type="email" id="email" name="email" value="<%= userJPA.getEmail()%>"/></td></tr>  
					<tr>
						<td>Sex:</td>
						<td>
							<input type="radio" name="sex" id="female" value="female" <%=(userJPA.getSex().equalsIgnoreCase("female")?"checked='checked'":"") %> />Female
							<input type="radio" name="sex" id="male" value="male" <%=(userJPA.getSex().equalsIgnoreCase("male")?"checked='checked'":"") %> />Male
						</td>
					</tr>  
					<tr>
						<td>Country:</td>
						<td>
							<select id="country" name="country" style="width:155px">  
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Peru")?"selected='selected'":"") %>>Peru</option>
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Argentina")?"selected='selected'":"") %>>Argentina</option>
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Bolivia")?"selected='selected'":"") %>>Bolivia</option>  
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Uruguay")?"selected='selected'":"") %>>Uruguay</option>  
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Chile")?"selected='selected'":"") %>>Chile</option>  
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Brazil")?"selected='selected'":"") %>>Brazil</option>
								<option <%=(userJPA.getCountry().equalsIgnoreCase("Other")?"selected='selected'":"") %>>Other</option>  
							</select>
						</td>
					</tr>
					<tr align="center">
						<td><input type="submit" value="Edit User"/></td>
						<td><button onclick="location.href='../index.jsp'" type="button">Cancel</button></td>
					</tr>  
				</table>  
			</form>
		</div>
	</body>
</html>