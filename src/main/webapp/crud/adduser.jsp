<%@page import="br.com.papodecafeteria.dao.jpa.UserDaoJpa"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>  
<jsp:useBean id="pUser" class="br.com.papodecafeteria.dao.model.UserJPA"></jsp:useBean>  
<jsp:setProperty property="*" name="pUser"/>  

<%
	int status = 0;

	try{
		status = UserDaoJpa.save(pUser);
	} catch (Exception exc){
		Logger.getLogger("adduser").log(Level.SEVERE, exc.getMessage());
	}

	if(status > 0){  
		response.sendRedirect("adduser-success.jsp");  
	}else{  
		response.sendRedirect("adduser-error.jsp");  
	}  
%>