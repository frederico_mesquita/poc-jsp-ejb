<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
		    $(".link").click(function(){
		        $("#home").fadeOut();
		    });
		});
		</script>
		<title>JSP/JPA Web Application Example</title>
	</head>
	<body>
		<div align="center" id="home">
			<p>
				<h1>JSP/JPA CRUD Example</h1>
				<button id="adduser" onclick="location.href='./crud/adduserform.jsp'" type="button">Add User</button>
				<button id="viewusers" onclick="location.href='./crud/viewusers.jsp'" type="button">View Users</button>
			</p>
			<p>
			    <address>
			        Written by <a href="mailto:frederico_mesquita@hotmail.com">Frederico Mesquita</a>.<br> 
			        Contact us at: Post Box 210, Belo Horizonte/MG Brazil
			    </address>
			</p>
		</div>
	</body>
</html>